//
//  ApiServiceTests.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 07/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
@testable import SimpleNewsApp

// TODO: Move it to helpers group as a separate file
extension NSHTTPURLResponse {
    convenience init?(statusCode: Int) {
        self.init(URL: NSURL(), statusCode: statusCode,
                  HTTPVersion: nil, headerFields: nil)
    }
}


class MockURLSessionDataTask: URLSessionDataTaskProtocol {
    var wasCalled: Bool = false
    
    func resume() {
        self.wasCalled = true
    }
}

class MockURLSession: URLSessionProtocol {
    var data: NSData?
    var error: NSError?
    var response: NSURLResponse?
    var urlRequest: NSURLRequest?
    var dataTask: MockURLSessionDataTask!
    
    func dataTaskWithRequest(request: NSURLRequest, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void) -> URLSessionDataTaskProtocol {
        self.urlRequest = request
        completionHandler(data, response, error)
        return dataTask
    }
}
