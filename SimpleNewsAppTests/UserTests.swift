//
//  UserTests.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 07/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import XCTest
@testable import SimpleNewsApp

class UserTests: XCTestCase {
    
    // FIXME: Change important properties like mail
    let correctJSON: JsonObject = [
        "objectId": "AmvQN2f6AA",
        "email": "pstanczak2@op.pl",
        "username": "pss",
        "createdAt": "2016-07-01T12:07:04.778Z",
        "updatedAt": "2016-07-01T12:07:04.778Z",
        "sessionToken": "r:945ee7ea90799be0931f164cce278e75"
    ]
    
    let incorrectJSON: JsonObject = [
        "email": "pstanczak2@op.pl",
        "username": "pss",
        "createdAt": "2016-07-01T12:07:04.778Z",
        "updatedAt": "2016-07-01T12:07:04.778Z",
        "sessionToken": "r:945ee7ea90799be0931f164cce278e75"
    ]
    
    override func setUp() {
        super.setUp()
        
        
        User.deleteAll(withPersistentStoreCoordinator: CoreDataManager.sharedManager().persistentStoreCoordinator, fromContext: User.context())
        _ = try? User.context().save()
    }
    
    func testAttributesSuccess() {
        let user = User.create(withJSON: correctJSON)
        XCTAssertNotNil(user)
        XCTAssertEqual("pss", user!.username)
    }
    
    func testAttributesFailure() {
        let user = User.create(withJSON: incorrectJSON)
        XCTAssertNil(user)
    }
    
    func testSessionToken() {
        let user = User.create(withJSON: correctJSON)
        XCTAssertEqual("r:945ee7ea90799be0931f164cce278e75", user!.sessionToken)
    }
    
    func testDeletingUser() {
        var user = User.create(withJSON: correctJSON)
        
        let isDeleted = user!.deleteUser()
        
        XCTAssertNil(user!.managedObjectContext)
        XCTAssertTrue(isDeleted)
        
        user = nil
        XCTAssertNil(user)
    }
    
    func testDeletingUserSessionToken() {
        let user = User.create(withJSON: correctJSON)
        let username = user!.username
        let isDeleted = user!.deleteUser()
        let token = KeychainService.loadToken(forUser: username)
        
        XCTAssertTrue(isDeleted)
        XCTAssertNil(user!.sessionToken)
        XCTAssertNil(token)
    }
    
    // Mark: Help methods
    
    static func deleteAllUsers() {
        (try? User.fetch())?.delete()
    }
}