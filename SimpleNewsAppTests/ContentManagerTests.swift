//
//  ContentManagerTests.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 08/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import XCTest
import CoreData

@testable import SimpleNewsApp

class ContentManagerTests: XCTestCase {
    var content: ContentManager!
    var service: ApiManager!
    let session = MockURLSession()
    let dataTask = MockURLSessionDataTask()
    
    override func setUp() {
        super.setUp()
        
        service = ApiManager(session: session)
        session.dataTask = dataTask
        session.response = NSHTTPURLResponse(statusCode: 200)
        content = ContentManager(apiManager: service)
        
        ArticleTests.deleteAllArticles()
    }
    
    // MARK: Get Article tests
    
    func testGetArticlesWithEmptyResults() {
        // given
        let mockData = MockData.emptyResultsData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        
        // when
        var getArticles: [Article]?
        content.getArticles({ articles in
            getArticles = articles
        }) { _ in }
        
        // then
        XCTAssertNotNil(getArticles)
        XCTAssert(getArticles!.count == 0)
    }
    
    func testGetArticlesWithIncorrectData() {
        // given
        let mockData = MockData.wrongData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        
        // when
        var getArticles: [Article]?
        content.getArticles({ articles in
            getArticles = articles
        }) { _ in }
        
        // then
        XCTAssertNotNil(getArticles)
        XCTAssert(getArticles!.count == 0)
    }
    
    func testGetArticlesWithCorrectData() {
        // given
        let mockData = MockData.resultData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        session.response = NSHTTPURLResponse(statusCode: 200)
        
        // when
        var getArticles: [Article]?
        content.getArticles({ articles in
            getArticles = articles
        }) { _ in }
        
        // then
        XCTAssertNotNil(getArticles)
        XCTAssert(getArticles!.count == 2)
    }
    
    func testGetArticlesWithApiErrorAndDBResults() {
        // given
        let numOfArticles = 3
        let articles = MockData.generateArticleJsonObjects(numOfArticles)
        let _ = try? [Article].create(articles)
        session.error = NSError(domain: "testGetArticlesWithError", code: 0, userInfo: nil)
        session.response = NSHTTPURLResponse(statusCode: 400)
        
        // when
        var getArticles: [Article]?
        content.getArticles({ articles in
            getArticles = articles
        }) { _ in }
        
        // then
        XCTAssertNotNil(getArticles)
        XCTAssert(getArticles!.count == numOfArticles)
    }
    
    func testGetArticlesWithApiErrorAndDBError() {
        // given
        session.error = NSError(domain: "testGetArticlesWithApiErrorAndDBError", code: 0, userInfo: nil)
        session.response = NSHTTPURLResponse(statusCode: 400)
        let dbManager = MockDataBaseManager()
        content = ContentManager(apiManager: service, dbManager: dbManager)
        
        // when
        var getError: NSError?
        content.getArticles({ articles in
            log("")
        }) { error in
            getError = error
        }
        
        // then
        XCTAssertNotNil(getError)
    }
 
    // MARK: Search tests
    
    func testSearchWithEmptyResults() {
        // given
        let mockData = MockData.emptyResultsData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        
        // when
        var getArticles: [Search]?
        
        content.search(withPhrase: "", success: { articles in
            getArticles = articles
        }) { _ in }
        
        // then
        XCTAssertNotNil(getArticles)
        XCTAssert(getArticles!.count == 0)
    }
    
    func testSearchWithIncorrectDataAndErrorResult() {
        // given
        let mockData = MockData.wrongData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        
        // when
        var getError: NSError?
        content.search(withPhrase: "", success: { _ in}) { error in
            getError = error
        }
        
        // then
        XCTAssertNotNil(getError)
    }
    
    func testSearchWithCorrectData() {
        // given
        let mockData = MockData.resultData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        session.response = NSHTTPURLResponse(statusCode: 200)
        
        // when
        var getArticles: [Search]?
        content.search(withPhrase: "", success: { articles in
            getArticles = articles
        }) { _ in }
        
        // then
        XCTAssertNotNil(getArticles)
        XCTAssert(getArticles!.count == 2)
    }
    
    // MARK: Favourites tests
    
    func testIsFavouriteWithNotLoggedUser() {
        // given
        UserTests.deleteAllUsers()
        
        // when
        var getError: NSError?
        content.isFavourite("", success: { _ in }) { error in
            getError = error
        }
        
        // then
        XCTAssertNotNil(getError)
        XCTAssert(getError!.code == 11)
    }
    
    func testIsFavouriteWithUserLoggedInAndEmptyResultData() {
        // given
        let _ = User.create(withJSON: MockData.correctUserJsonData)
        let mockData = MockData.emptyResultsData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        
        // when
        var getFavourite: Favourite?
        content.isFavourite("", success: { favourite in
            getFavourite = favourite
        }) { _ in }
        
        // then
        XCTAssertNil(getFavourite)
    }
    
    func testIsFavouriteWithUserLoggedInAndIncorectResultData() {
        // given
        let _ = User.create(withJSON: MockData.correctUserJsonData)
        let mockData = MockData.wrongData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        
        // when
        var getError: NSError?
        content.isFavourite("", success: { _ in }) { error in
            getError = error
        }
        
        // then
        XCTAssertNotNil(getError)
    }
    
    func testIsFavouriteWithUserLoggedInAndCorectResultData() {
        // given
        let _ = User.create(withJSON: MockData.correctUserJsonData)
        let mockData = MockData.favouriteResultsStringData
        session.data = mockData.dataUsingEncoding(NSUTF8StringEncoding)
        
        // when
        var getFavourite: Favourite?
        content.isFavourite("", success: { favourite in
            getFavourite = favourite
        }) { _ in }
        
        // then
        XCTAssertNotNil(getFavourite)
    }

}