//
//  MockDataBaseManager.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 12/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
@testable import SimpleNewsApp

class MockDataBaseManager: DataBaseManagerProtocol {
    
    func addArticles(withJSON JSON: JsonObject) -> [Article]? {
        return nil        
    }
    
    func getArticles() -> [Article]? {
        return nil
    }
}