//
//  ArticleTests.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 24/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import XCTest
@testable import SimpleNewsApp

class ArticleTests: XCTestCase {
    
    let correctJSON: JsonObject = [
        "objectId": "i0XQrJ35II",
        "title": "Title 1",
        "updatedAt": "2016-06-24T08:17:15.357Z",
        "createdAt": "2016-06-24T08:17:02.767Z",
        "lead": "Lead 1",
        "content": "Content 1"
    ]
    
    let incorrectJSON: JsonObject = [
        "title": "Title 1",
        "updatedAt": "2016-06-24T08:17:15.357Z",
        "createdAt": "2016-06-24T08:17:02.767Z",
        "lead": "Lead 1",
        "content": "Content 1"
    ]
    
    override func setUp() {
        super.setUp()
        
        
        Article.deleteAll(withPersistentStoreCoordinator: CoreDataManager.sharedManager().persistentStoreCoordinator, fromContext: Article.context())
        _ = try? Article.context().save()
    }
    
    func testAttributesSuccess() {
        
        let JSON = [
            "objectId": "i0XQrJ35II",
            "title": "Title 1",
            "updatedAt": "2016-06-24T08:17:15.357Z",
            "createdAt": "2016-06-24T08:17:02.767Z",
            "lead": "Lead 1",
            "content": "Content 1"
        ]
        
        let article = Article.create(withJSON: JSON)
        XCTAssertNotNil(article)
        XCTAssertEqual("Title 1", article!.title)
    }
    
    func testAttributesFailure() {
        
        let JSON = [
            "title": "Title 1",
            "updatedAt": "2016-06-24T08:17:15.357Z",
            "createdAt": "2016-06-24T08:17:02.767Z",
            "lead": "Lead 1",
            "content": "Content 1"
        ]
        
        let article = Article.create(withJSON: JSON)
        XCTAssertNil(article)
    }
    
    func testCreateArticlesWithGoodAndBad() {
        let articles = [correctJSON, incorrectJSON]
        let items = try? [Article].create(articles)
        
        XCTAssertEqual(1, items!.count)
    }
    
    func testCreateArticlesWithOnlyGood() {
        let articles = [correctJSON, correctJSON]
        let items = try? [Article].create(articles)
        
        XCTAssertEqual(2, items!.count)
    }
    
    func testCreateArticlesWithOnlyBad() {
        let articles = [incorrectJSON, incorrectJSON]
        let items = try? [Article].create(articles)
        
        XCTAssertEqual(0, items!.count)
    }
    
    // Mark: Help methods
    
    static func deleteAllArticles() {
        
        (try? Article.fetch())?.delete()
        
        /*if let toDelete: [Article] = try? Article.fetch() {
         toDelete.delete()
         }*/
        
        // For unknown reasons, it doeasn't work in tests
        //let context = Article.context()
        //context.mergePolicy = NSOverwriteMergePolicy
        //Article.deleteAll(withPersistentStoreCoordinator: CoreDataManager.sharedManager().persistentStoreCoordinator, fromContext: Article.context())
        //CoreDataManager.sharedManager().saveContext()
    }
}