//
//  FavouriteTests.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 07/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

import Foundation
import XCTest
@testable import SimpleNewsApp

class FavouriteTests: XCTestCase {
    
    let correctJSON: JsonObject = [
        "objectId": "n6POERQcDk",
        "userId": "AmvQN2f6AA",
        "articleId": "i0XQrJ35II",
        "createdAt": "2016-07-04T13:48:40.779Z",
        "updatedAt": "2016-07-04T13:48:40.779Z"
    ]
    
    let incorrectJSON: JsonObject = [
        "userId": "AmvQN2f6AA",
        "articleId": "i0XQrJ35II",
        "createdAt": "2016-07-04T13:48:40.779Z",
        "updatedAt": "2016-07-04T13:48:40.779Z"
    ]
    
    func testAttributesSuccess() {
        let favourite = Favourite.create(withJSON: correctJSON)
        XCTAssertNotNil(favourite)
        XCTAssertEqual("i0XQrJ35II", favourite!.articleId)
    }
    
    func testAttributesFailure() {
        let favourite = Favourite.create(withJSON: incorrectJSON)
        XCTAssertNil(favourite)
    }
    
    func testCreatingFavouritesWithCorectObjects() {
        let favouriteDicts = [correctJSON, correctJSON]
        let favouriteItems = try? [Favourite].create(favouriteDicts)
        
        XCTAssertEqual(2, favouriteItems!.count)
    }
    
    func testCreatingFavouritesWithIncorectObjects() {
        let favouriteDicts = [incorrectJSON, incorrectJSON]
        let favouriteItems = try? [Favourite].create(favouriteDicts)
        
        XCTAssertEqual(0, favouriteItems!.count)
    }
    
    func testCreatingFavouritesWithCorrectAndIncorrectObjects() {
        let favouriteDicts = [incorrectJSON, correctJSON]
        let favouriteItems = try? [Favourite].create(favouriteDicts)
        
        XCTAssertEqual(1, favouriteItems!.count)
    }

}