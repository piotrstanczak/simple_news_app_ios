//
//  ApiServiceTests.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 07/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import XCTest
@testable import SimpleNewsApp

class ApiServiceTests: XCTestCase {
    var service: ApiManager!
    let session = MockURLSession()
    let dataTask = MockURLSessionDataTask()
    
    override func setUp() {
        super.setUp()
        
        service = ApiManager(session: session)
        session.dataTask = dataTask
    }
    
    func testRequestWasCalled() {
        // given
        
        // when
        service.perform(withProvider: Provider.Article.All(), success: { _ in }) { _ in }
        
        // then
        XCTAssert(dataTask.wasCalled)
        XCTAssertEqual(session.urlRequest?.URL, Provider.Article.All().URLRequest.URL)
    }
    
    func testRequestResponseDataThatReturnsTheData() {
        // given
        let expectedData = "{}".dataUsingEncoding(NSUTF8StringEncoding)
        session.data = expectedData
        session.response = NSHTTPURLResponse(statusCode: 200)
        
        // when
        var data: JsonObject?
        service.perform(withProvider: Provider.Article.All(), success: { json in
            data = json
        }) { _ in }
        
        // then
        XCTAssertNotNil(data)
    }
    
    func testRequestResponseDataThatReturnsTheError() {
        // given
        session.error = NSError(domain: "error", code: 0, userInfo: nil)
        
        // when
        var getError: NSError?
        service.perform(withProvider: Provider.Article.All(), success: { _ in }) { error in
            getError = error
        }
        
        // then
        XCTAssertNotNil(getError)
    }
    
    func testRequestResponseDataWithStatusCodeLessThan200AsError() {
        // given
        session.response = NSHTTPURLResponse(statusCode: 199)
        
        // when
        var getError: NSError?
        service.perform(withProvider: Provider.Article.All(), success: { _ in }) { error in
            getError = error
        }
        // then
        XCTAssertNotNil(getError)
    }
    
    func testRequestResponseDataWithStatusCodeGreaterThan299AsError() {
        // given
        session.response = NSHTTPURLResponse(statusCode: 299)
        
        // when
        var getError: NSError?
        service.perform(withProvider: Provider.Article.All(), success: { _ in }) { error in
            getError = error
        }
        
        // then
        XCTAssertNotNil(getError)
    }
    
    // TODO: Move it up, to the ContentManager/UserManager tests layer
    
    func testRequestReturnsTrueData() {
        // given
        service = ApiManager()
        let provider = Provider.Article.All()
        let expectation = expectationWithDescription("Wait for \(provider.pathURL) to load.")
        
        // when
        var data: JsonObject?
        service.perform(withProvider: provider, success: { json in
            data = json
            expectation.fulfill()
        }) { error in
            expectation.fulfill()
        }
        
        // then
        waitForExpectationsWithTimeout(5, handler: nil)
        XCTAssertNotNil(data)
    }
}