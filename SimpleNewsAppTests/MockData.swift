//
//  MockData.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 12/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
@testable import SimpleNewsApp

class MockData {
    
    static let resultData = "{\"results\": [" +
        "{" +
        "\"objectId\": \"i0XQrJ35II\"," +
        "\"title\": \"Title 1\"," +
        "\"updatedAt\": \"i0XQrJ35II\"," +
        "\"createdAt\": \"i0XQrJ35II\"," +
        "\"lead\": \"Lead 1\"," +
        "\"content\": \"Content 1\"" +
        "}," +
        "{" +
        "\"objectId\": \"i0XQrJ35II\"," +
        "\"title\": \"Title 2\"," +
        "\"updatedAt\": \"i0XQrJ35II\"," +
        "\"createdAt\": \"i0XQrJ35II\"," +
        "\"lead\": \"Lead 2\"," +
        "\"content\": \"Content 2\"" +
        "}" +
    "]}"
    
    static let favouriteResultsStringData = "{\"results\": [" +
    "{" +
        "\"articleId\": \"i0XQrJ35II\"," +
        "\"createdAt\": \"2016-07-04T13:48:40.779Z\"," +
        "\"objectId\": \"n6POERQcDk\"," +
        "\"updatedAt\": \"2016-07-04T13:48:40.779Z\"," +
        "\"userId\": \"AmvQN2f6AA\"" +
    "}" +
    "]}"
    
    static let wrongData = "{[]}"
    static let emptyResultsData = "{\"results\": []}"
    
    static let correctArticleJsonData: JsonObject = [
        "objectId": "i0XQrJ35II",
        "title": "Title 1",
        "updatedAt": "2016-06-24T08:17:15.357Z",
        "createdAt": "2016-06-24T08:17:02.767Z",
        "lead": "Lead 1",
        "content": "Content 1"
    ]
    
    static let correctUserJsonData: JsonObject = [
        "objectId": "AmvQN2f6AA",
        "email": "pstanczak2@op.pl",
        "username": "pss",
        "createdAt": "2016-07-01T12:07:04.778Z",
        "updatedAt": "2016-07-01T12:07:04.778Z",
        "sessionToken": "r:945ee7ea90799be0931f164cce278e75"
    ]
    
    static func generateArticleJsonObjects(number: Int) -> [JsonObject] {
        var objects = [JsonObject]()
        for i in 0..<number {
            let article = [
                "objectId": "\(i)",
                "title": "Title \(i)",
                "updatedAt": "2016-06-24T08:17:15.357Z",
                "createdAt": "2016-06-24T08:17:02.767Z",
                "lead": "Lead \(i)",
                "content": "Content \(i)"
            ]
            objects.append(article)
        }
        return objects
    }
}