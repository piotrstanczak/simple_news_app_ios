//
//  KeychainServiceTests.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 07/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import XCTest
@testable import SimpleNewsApp

class KeychainServiceTests: XCTestCase {
    
    let user = "user"
    let token = "token1234"
    
    func testSavingWithSuccess() {
        KeychainService.saveToken(token, forUser: user)
        let loadedToken = KeychainService.loadToken(forUser: user)
        XCTAssertEqual(loadedToken, token)
    }
    
    func testDeletingWithSuccess() {
        KeychainService.saveToken(token, forUser: user)
        var loadedToken = KeychainService.loadToken(forUser: user)
        log("loadedToken: \(loadedToken)")
        XCTAssertNotNil(loadedToken)
        KeychainService.deleteToken(loadedToken!, forUser: user)
        
        loadedToken = KeychainService.loadToken(forUser: user)
        XCTAssertNil(loadedToken)
    }
}
