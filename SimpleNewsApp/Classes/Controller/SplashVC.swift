//
//  SplashVC.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 06/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit

class SplashVC: UIViewController {
    
    // MARK: View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
    }
    
    // MARK: Data handlers
    
    func getData() {
        UserManager.sharedInstance().validateSessionToken({ [weak self] success in
            log("User logged in!")
            self?.showFeed()
        }) { [weak self] error in
            log("User not logged: \(error)")
            self?.showFeed()
        }
    }
    
    func showFeed() {
        dispatch_async(dispatch_get_main_queue()) { 
            self.performSegueWithIdentifier("FeedShow", sender: self)
        }
    }
}