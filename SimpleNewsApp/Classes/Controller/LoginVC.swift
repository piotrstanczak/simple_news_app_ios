//
//  LoginVC.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 28/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit

class LoginVC: UIViewController {
    
    // MARK: IB
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logoutButton: UIBarButtonItem!
    
    // MARK: View life cycle
    
    override func viewDidLoad() {
        self.loginTextField.text = "pss"
        self.passwordTextField.text = "piotr123"
    }
    
    // MARK: Action handlers
    
    @IBAction func cancelButtonHandler(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func logInButtonHandler(sender: AnyObject) {
        
        guard let username = self.loginTextField.text, password = self.passwordTextField.text where username.characters.count > 0 else  { return }
        
        UserManager.sharedInstance().logIn(withUsername: username, password: password, success: { (user) in
            log("user: \(user.id)")
            log("user: \(user.username)")
            log("user: \(user.sessionToken)")
        }, failure: { (error) in
            
        })        
    }
    
    @IBAction func deleteButtonHandler(sender: AnyObject) {
        UserManager.sharedInstance().delete({ success in
            
        }) { error in
            
        }
    }
    
    @IBAction func logoutButtonHandler(sender: AnyObject) {
        UserManager.sharedInstance().logOut({ success in
            
        }) { error in
            
        }
    }
    
}