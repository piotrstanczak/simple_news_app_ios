//
//  ArticleDetailVC.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 27/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit

class FeedDetailVC: UIViewController {
    
    // MARK: Private properties
    
    private var _article: IArticle!
    private var _favourite: Favourite? {
        didSet {
            dispatch_async(dispatch_get_main_queue()) {
                if self._favourite == nil {
                    self.favouriteBarButtonItem?.image = self.articleView.favouriteIcon
                } else {
                    self.favouriteBarButtonItem?.image = self.articleView.favouriteMarkedIcon
                }
            }
        }
    }
    
    // MARK: Public properties
    
    var articleView: ArticleView! {
        return self.view as! ArticleView
    }
    
    var favouriteBarButtonItem: UIBarButtonItem?
    
    // MARK: View life cycle
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.favouriteBarButtonItem = navigationItem.rightBarButtonItem
        self.isFavourite()
    }
    
    func setupWithArticle(article: IArticle) {
        self._article = article
        self.articleView.webView.loadHTMLString(article.content, baseURL: nil)
    }
    
    func setupFavouriteIcon(marked: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            if marked {
                self.favouriteBarButtonItem?.image = self.articleView.favouriteMarkedIcon
            } else {
                self.favouriteBarButtonItem?.image = self.articleView.favouriteIcon
            }
        }
    }
    
    // MARK: Action handlers
    
    @IBAction func addToFavouritesButtonHandler(sender: AnyObject) {
        if _favourite == nil {
            self.addToFavourite()
        } else {
            self.deleteFromFavorourite(withId: _favourite!.id)
        }
    }
    
    // MARK: Data handlers
    
    func isFavourite() {
        ContentManager().isFavourite(_article.id, success: { favourite in
            self._favourite = favourite
        }) { error in
            log("\(error.localizedDescription)")
        }
    }
    
    func addToFavourite() {
        ContentManager().addToFavourite(self._article.id, success: { favourite in
            self._favourite = favourite
        }) { error in
            log("error: \(error.localizedDescription)")
            let alert = UIAlertController(title: "Ups", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func deleteFromFavorourite(withId id: String) {
        ContentManager().removeFromFavourite(id, success: {
            self._favourite = nil
        }) { error in
            log("error: \(error.localizedDescription)")
            let alert = UIAlertController(title: "Ups", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}