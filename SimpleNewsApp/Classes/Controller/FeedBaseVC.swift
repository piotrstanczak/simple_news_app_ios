//
//  FeedBaseVC.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 24/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import UIKit

enum FeedType {
    case Regular
    case Favourite
}

class FeedBaseVC: UIViewController {

    // MARK: Private properteis
    
    internal var _articles: [IArticle] = [] {
        didSet {
            log("\(_articles.count)")
            dispatch_async(dispatch_get_main_queue()) {
                self.feedView.tableView.reloadData()
            }
        }
    }
    
    private var _selectedArticleIndex: Int?
    
    // MARK: public properteis
    
    var feedView: FeedView! {
        return self.view as! FeedView
    }
    
    internal var profileBarButtonItem: UIBarButtonItem?
    internal var searchBarButtonItem: UIBarButtonItem?
    
    internal var type: FeedType = .Regular
    
    // MARK: View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    // MARK: View setup
    
    internal func setupView() {
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationItem.titleView = self.feedView.logoImageView
        
        self.feedView.tableView.delegate = self
        self.feedView.tableView.dataSource = self
    }
        
    // MARK: Data handlers
    
    func updateView(withData data: [IArticle]) {
        self._articles = data
    }
}

extension FeedBaseVC {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FeedDetailShow", let selectedArticleIndex = _selectedArticleIndex {
            
            let article = _articles[selectedArticleIndex]
            
            let articleDetailVC = segue.destinationViewController as! FeedDetailVC
            let _ = articleDetailVC.view
            
            articleDetailVC.setupWithArticle(article)
        }
    }
}

// MARK: UITableViewDataSource delegate handlers

extension FeedBaseVC: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _articles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let article: IArticle = _articles[indexPath.row]
        
        let cell = self.feedView.tableView.dequeueReusableCellWithIdentifier("FeedBasicCell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = article.title
        return cell
    }
}

// MARK: UITableViewDelegate delegate handlers

extension FeedBaseVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        _selectedArticleIndex = indexPath.row
        self.performSegueWithIdentifier("FeedDetailShow", sender: self)
    }
}