//
//  FeedRegularVC.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 05/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit

class FeedRegularVC: FeedBaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = .Regular
        self.getData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setupUserBarButton()
    }
    
    override func setupView() {
        super.setupView()
        
        self.feedView.setupSearchBar()
        self.feedView.searchBar.delegate = self
        
        self.profileBarButtonItem = navigationItem.leftBarButtonItem
        self.searchBarButtonItem = navigationItem.rightBarButtonItem
    }
    
    func setupUserBarButton() {
        if UserManager.sharedInstance().user == nil {
            self.profileBarButtonItem?.image = self.feedView.userNotLoggedIcon
        } else {
            self.profileBarButtonItem?.image = self.feedView.userLoggedIcon
        }
    }
    
    // MARK: Data handlers
    
    func getData() {
        ContentManager().getArticles({ [weak self] articles in
            if let articles = articles {
                self?.updateView(withData: articles.map{ $0 as IArticle })
            }
        }) { error in
            log("Error: \(error.localizedDescription)")
        }
    }
    
    func getData(withPhrase phrase: String) {
        ContentManager().search(withPhrase: phrase, success: { [weak self] articles in
            let newArticles = articles.map{ $0 as IArticle }
            self?.updateView(withData: newArticles)
        }) { error in
            log("Error: \(error.localizedDescription)")
        }
    }
    
    // MARK: Action handlers
    
    @IBAction func searchButtonHandler(sender: AnyObject) {
        _articles = []
        showSearchBar()
    }
}

// MARK: UISearchBarDelegate

extension FeedRegularVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if let phrase = searchBar.text {
            self.getData(withPhrase: phrase)
        }
    }
    
    func hideSearchBar() {
        self.feedView.logoImageView.alpha = 0
        navigationItem.setRightBarButtonItem(searchBarButtonItem, animated: true)
        navigationItem.setLeftBarButtonItem(profileBarButtonItem, animated: true)
        UIView.animateWithDuration(0.3, animations: {
            self.navigationItem.titleView = self.feedView.logoImageView
            self.feedView.logoImageView.alpha = 1
            }, completion: { finished in
                self.getData()
        })
    }
    
    func showSearchBar() {
        self.feedView.searchBar.alpha = 0
        navigationItem.titleView = self.feedView.searchBar
        navigationItem.setLeftBarButtonItem(nil, animated: true)
        navigationItem.setRightBarButtonItem(nil, animated: true)
        UIView.animateWithDuration(0.5, animations: {
            self.feedView.searchBar.alpha = 1
            }, completion: { finished in
                self.feedView.searchBar.becomeFirstResponder()
        })
    }
}