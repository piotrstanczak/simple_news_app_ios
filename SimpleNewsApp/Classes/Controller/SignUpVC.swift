//
//  SignUpVC.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 29/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit

class SignUpVC: UIViewController {
    
    // MARK: IB
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // MARK: View life cycle
    
    override func viewDidLoad() {
        self.emailTextField.text = "pstanczak2@op.pl"
        self.usernameTextField.text = "pss"
        self.passwordTextField.text = "piotr123"
    }
    
    
    // MARK: Action handlers
    
    @IBAction func signUpHandler(sender: AnyObject) {
        guard let email = self.emailTextField.text, username = self.usernameTextField.text, password = self.passwordTextField.text where username.characters.count > 0 else   { return }
        
        UserManager.sharedInstance().signUp(withEmail: email, username: username, password: password, success: { user in
                
        }) { error in
            
        }
    }
    
}