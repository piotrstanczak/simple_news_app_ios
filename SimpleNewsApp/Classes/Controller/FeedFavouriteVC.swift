//
//  FavouriteFeedVC.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 04/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

class FeedFavouriteVC: FeedBaseVC {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.getData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.type = .Favourite
    }
    
    // MARK: Data handlers
    
    func getData() {
        ContentManager().getFavourites({ [weak self] articles in
            if let articles = articles {
                log("articles: \(articles)")
                self?.updateView(withData: articles.map{ $0 as IArticle })
            }
        }) { error in
            log("Error: \(error.localizedDescription)")
        }
    }
}