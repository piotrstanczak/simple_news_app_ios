//
//  UserManager.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 28/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

class UserManager {
    
    // MARK: Private properteis
    private static let _instance = UserManager()
    
    // MARK: Public properties
    var _user: User?
    var user: User? {
        get {
            if _user == nil {
                _user = (try? User.fetch(withLimit: 1))?.first
            }
            
            return _user
        }
        
        set (newValue) {
            if _user != nil && newValue == nil {
                let deleted = _user?.deleteUser()
                log("User deleted? \(deleted)")
            }
            
            _user = newValue
        }
    }
    
    static func sharedInstance() -> UserManager {
        return _instance
    }
    
    // TODO: Move to public error enum
    lazy var userNotLoggedError: NSError = {
        var temporaryError = NSError(domain: "com.flymeapp", code: 11, userInfo: [:])
        return temporaryError
    }()
    
    func logIn(withUsername username: String, password: String, success: (user: User) -> (), failure: (error: NSError) -> ()) {
        
        ApiManager().perform(withProvider: Provider.User.LogIn(username: username, password: password), success: { JSON in
            if let user = User.create(withJSON: JSON) {
                self.user = user
                success(user: user)
            } else {
                let error = NSError(domain: "com...", code: 42, userInfo: [:])
                failure(error: error)
            }
        }) { error in
            failure(error: error)
        }
    }
    
    func logOut(success: Bool -> (), failure: Failure) {
        guard let user = user, let token = user.sessionToken else {
            failure(userNotLoggedError)
            return
        }
        
        ApiManager().perform(withProvider: Provider.User.LogOut(sessionToken: token), success: { JSON in
            log("json: \(JSON)")
            
            self.user = nil
            success(true)
        }) { error in
            failure(error)
        }
    }
    
    func signUp(withEmail email: String, username: String, password: String, success: (user: User) -> (), failure: (error: NSError) -> ()) {
        ApiManager().perform(withProvider: Provider.User.SignUp(email: email, username: username, password: password), success: { json in
            if let user = User.create(withJSON: json) {
                self.user = user
                success(user: user)
            } else {
                let error = NSError(domain: "com...", code: 42, userInfo: [:])
                failure(error: error)
            }
        }) { error in
            failure(error: error)
        }
    }
    
    func delete(success: Bool -> (), failure: Failure) {
        guard let user = user, let token = user.sessionToken else {
            failure(userNotLoggedError)
            return
        }
        
        ApiManager().perform(withProvider: Provider.User.Delete(id: user.id, sessionToken: token), success: { json in
            log("json: \(json)")
            self.user = nil
            success(true)
        }) { error in
            failure(error)
        }
    }
    
    func validateSessionToken(success: (Bool) -> (), failure: Failure) {
        
        // FIXME: Add username to loadtoken method
        guard let user = user, let token = user.sessionToken else {
            failure(userNotLoggedError)
            return
        }
        
        log("token: \(token)")
        
        ApiManager().perform(withProvider: Provider.User.Validate(sessionToken: token), success: { json in
            log("json: \(json)")
            
            if let code = json["code"] as? Int where code == 209 {
                self.user = nil
                failure(self.userNotLoggedError)
            } else {
                success(true)
            }
            
        }, failure: failure)
    }
}