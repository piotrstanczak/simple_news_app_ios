//
//  DataBaseManagerProtocol.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 12/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

protocol DataBaseManagerProtocol {
    func addArticles(withJSON JSON: JsonObject) -> [Article]?
    func getArticles() -> [Article]?    
}