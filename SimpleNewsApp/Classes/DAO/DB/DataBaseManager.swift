//
//  DataBaseManager.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 24/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

final class DataBaseManager: DataBaseManagerProtocol {
    
    func addArticles(withJSON JSON: JsonObject) -> [Article]? {
        
        guard let articles = JSON["results"] as? [JsonObject] else {
            return nil
        }
        
        if articles.count > 0 {
            log("articles: \(articles.count)")
            let items = try? [Article].create(articles)
            log("items: \(items!.count)")
            return items
        } else {
            return []
        }
    }
    
    func getArticles() -> [Article]? {
        return try? Article.fetch()
    }
}