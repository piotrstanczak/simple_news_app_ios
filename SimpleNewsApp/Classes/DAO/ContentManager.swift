//
//  ContentManager.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 24/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

final class ContentManager {
    
    private let apiManager: ApiManagerProtocol
    private let dbManager: DataBaseManagerProtocol
    
    init(apiManager: ApiManagerProtocol = ApiManager(), dbManager: DataBaseManagerProtocol = DataBaseManager()) {
        self.apiManager = apiManager
        self.dbManager = dbManager
    }
    
    func getArticles(success: (articles: [Article]?) -> (), failure: (error: NSError) -> ()) {
        apiManager.perform(withProvider: Provider.Article.All(), success: { JSON in
            log("JSON: \(JSON)")
            let articles = self.dbManager.addArticles(withJSON: JSON)
            success(articles: articles)
        }) { error in
            if let articles = self.dbManager.getArticles() {
                success(articles: articles)
            } else {
                failure(error: error)
            }
        }
    }
    
    func search(withPhrase phrase: String, success: (articles: [Search]) -> (), failure: (error: NSError) -> ()) {
        apiManager.perform(withProvider: Provider.Article.Search(phrase: phrase), success: { json in
            var searchResults = [Search]()
            if let articles = json["results"] as? [JsonObject], items = try? [Search].create(articles) {
                log("articles: \(articles.count)")
                log("items: \(items.count)")
                searchResults = items
            }
            
            success(articles: searchResults)
            
        }) { error in
            failure(error: error)
        }
    }
    
    func isFavourite(articleId: String, success: (favourite: Favourite?) -> (), failure: (error: NSError) -> ()) {
        
        guard let user = UserManager.sharedInstance().user else {
            failure(error: UserManager.sharedInstance().userNotLoggedError)
            return
        }
        
        apiManager.perform(withProvider: Provider.Favourite.Is(userId: user.id, articleId: articleId), success: { json in
            
            var favourite: Favourite? = nil
            log("json: \(json)")
            if let articles = json["results"] as? [JsonObject] where articles.count > 0 {
                favourite = Favourite.create(withJSON: articles.first!)
                log("favourite: \(favourite)")
            }
            
            success(favourite: favourite)
            
        }) { error in
            failure(error: error)
        }
    }
    
    func addToFavourite(articleId: String, success: (Favourite) -> (), failure: (error: NSError) -> ()) {
        
        guard let user = UserManager.sharedInstance().user else {
            failure(error: UserManager.sharedInstance().userNotLoggedError)
            return
        }
        
        apiManager.perform(withProvider: Provider.Favourite.Add(userId: user.id, articleId: articleId), success: { json in
            
            log("json: \(json)")
            if let favourite = Favourite.create(withJSON: json) {
                success(favourite)
            } else {
                let error = NSError(domain: "com.", code: 12, userInfo: [:])
                failure(error: error)
            }
            
        }, failure: failure)
    }
    
    func removeFromFavourite(id: String, success: () -> (), failure: (error: NSError) -> ()) {
        
        guard let _ = UserManager.sharedInstance().user else {
            failure(error: UserManager.sharedInstance().userNotLoggedError)
            return
        }
        
        apiManager.perform(withProvider: Provider.Favourite.Remove(id: id), success: { json in
            
            log("json: \(json)")
            success()
            
        }, failure: failure)
    }
    
    func getFavourites(success: (articles: [Article]?) -> (), failure: (error: NSError) -> ()) {
        guard let user = UserManager.sharedInstance().user else {
            failure(error: UserManager.sharedInstance().userNotLoggedError)
            return
        }
        
        apiManager.perform(withProvider: Provider.Favourite.All(userId: user.id), success: { json in
            log("json: \(json)")
            
            if let favouriteObjects = json["results"] as? [JsonObject], favourites = try? [Favourite].create(favouriteObjects) {
                
                let ids = favourites.map{$0.articleId}
                ApiManager().perform(withProvider: Provider.Article.AllWithIds(ids: ids), success: { json in
                    log("json: \(json)")
                    
                    var articles: [Article]?
                    if let articleObjects = json["results"] as? [JsonObject] {
                        articles = try? [Article].create(articleObjects)
                    }
                    success(articles: articles)
                    
                }, failure: failure)
            }
        }, failure: failure)
    }
}