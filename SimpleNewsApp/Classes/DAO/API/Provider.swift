//
//  Provider.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 08/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

private let kBaseApi = "https://parseapi.back4app.com/"
// TODO: Move to Configuration.plist
private let kAppId = "r6qHm3COjbqXKt739Ofp5GKdSy3dpPaktmWfKHuL"
private let kMasterKey = "z7FTmRVz78Zx1jqxaS8oIu2KM9va7SnxxhN6uVVc"
private let kRestKey = "vIuqn8Nd6CI6ESucMzRAyXTEs8Eteflb43OLeQbK"

// TODO: Hide
public protocol Providable {
    var body: NSData? { get }
    var method: String { get }
    var pathURL: NSURL { get }
    var URLRequest: NSMutableURLRequest { get }
}

extension Providable {
    
    var body: NSData? {
        return nil
    }
    
    internal var baseURLRequest: NSMutableURLRequest {
        let request = NSMutableURLRequest()
        request.addValue(kAppId, forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue(kRestKey, forHTTPHeaderField: "X-Parse-REST-API-Key")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("1", forHTTPHeaderField: "X-Parse-Revocable-Session")
        
        request.HTTPMethod = method
        request.URL = pathURL
        
        if let body = self.body {
            request.HTTPBody = body
        }
        
        return request
    }
}

class Provider {
}

extension Provider {
    enum Article: Providable {
        case All()
        case AllWithIds(ids: [String])
        case Search(phrase: String)
        
        internal var pathURL: NSURL {
            switch self {
            case .All:
                return NSURL(string: kBaseApi + "classes/Article")!
                
            case let .Search(phrase):
                let urlComponents = NSURLComponents(string: kBaseApi + "classes/Article")!
                urlComponents.queryItems = [
                    NSURLQueryItem(name: "where", value: "{\"title\":{\"$regex\":\"\(phrase)\"}}")
                ]
                log("urlComponents.URL: \(urlComponents.URL!)")
                return urlComponents.URL!
            case let .AllWithIds(ids):
                let urlComponents = NSURLComponents(string: kBaseApi + "classes/Article")!
                let idsString = "[" + ids.map{"\"\($0)\""}.joinWithSeparator(",") + "]"
                log("idsString: \(idsString)")
                urlComponents.queryItems = [
                    NSURLQueryItem(name: "where", value: "{\"objectId\":{\"$in\":\(idsString)}}")
                ]
                log("urlComponents.URL: \(urlComponents)")
                return urlComponents.URL!
            }
        }
        
        internal var method: String {
            return "GET"
        }
        
        var URLRequest: NSMutableURLRequest {
            return self.baseURLRequest
        }
    }
    
    enum Favourite: Providable {
        case Is(userId: String, articleId: String)
        case Add(userId: String, articleId: String)
        case Remove(id: String)
        case All(userId: String)
        
        var pathURL: NSURL {
            switch self {
            case let .Is(userId, articleId):
                let urlComponents = NSURLComponents(string: kBaseApi + "classes/Favourite")!
                urlComponents.queryItems = [
                    NSURLQueryItem(name: "where", value: "{\"userId\":\"\(userId)\", \"articleId\":\"\(articleId)\"}"),
                    NSURLQueryItem(name: "count", value: "1"),
                    NSURLQueryItem(name: "limit", value: "1")
                ]
                return urlComponents.URL!
                
            case .Add:
                return NSURL(string: kBaseApi + "classes/Favourite")!
                
            case let .Remove(id):
                return NSURL(string: kBaseApi + "classes/Favourite/\(id)")!
                
            case let .All(userId):
                let urlComponents = NSURLComponents(string: kBaseApi + "classes/Favourite")!
                urlComponents.queryItems = [
                    NSURLQueryItem(name: "where", value: "{\"userId\":\"\(userId)\"}")
                ]
                return urlComponents.URL!
            }
        }
        
        var body: NSData? {
            switch self {
            case let .Add(userId, articleId):
                let params = ["userId": userId, "articleId": articleId]
                return try? NSJSONSerialization.dataWithJSONObject(params, options: [])
            default:
                return nil
            }
        }
        
        var method: String {
            switch self {
            case .All, .Is:         return "GET"
            case .Add:              return "POST"
            case .Remove:           return "DELETE"
            }
        }
        
        var URLRequest: NSMutableURLRequest {
            return self.baseURLRequest
        }
    }
    
    enum User: Providable {
        case SignUp(email: String, username: String, password: String)
        case LogIn(username: String, password: String)
        case LogOut(sessionToken: String)
        case Delete(id: String, sessionToken: String)
        case Validate(sessionToken: String)
        
        var pathURL: NSURL {
            switch self {
            case .SignUp:
                return NSURL(string: kBaseApi + "users")!
            case let .LogIn(username, password):
                let urlComponents = NSURLComponents(string: kBaseApi + "login")!
                urlComponents.queryItems = [
                    NSURLQueryItem(name: "username", value: username),
                    NSURLQueryItem(name: "password", value: password)
                ]
                return urlComponents.URL!
            case .LogOut:
                return NSURL(string: kBaseApi + "logout")!
            case let .Delete(id, _):
                return NSURL(string: kBaseApi + "users/\(id)")!
            case .Validate:
                return NSURL(string: kBaseApi + "users/me")!
            }
        }
        
        var method: String {
            switch self {
            case .SignUp:               return "POST"
            case .LogIn, .Validate:     return "GET"
            case .LogOut:               return "POST"
            case .Delete:               return "DELETE"
            }
        }
        
        var body: NSData? {
            switch self {
            case let .SignUp(email, username, password):
                let params = ["username": username, "password": password, "email": email]
                return try? NSJSONSerialization.dataWithJSONObject(params, options: [])
            default:
                return nil
            }
        }
        
        var URLRequest: NSMutableURLRequest {
            let request = self.baseURLRequest
            
            if case let .LogOut(sessionToken) = self {
                request.addValue(sessionToken, forHTTPHeaderField: "X-Parse-Session-Token")
            } else if case let .Delete(_, sessionToken) = self {
                request.addValue(sessionToken, forHTTPHeaderField: "X-Parse-Session-Token")
            } else if case let .Validate(sessionToken) = self {
                request.addValue(sessionToken, forHTTPHeaderField: "X-Parse-Session-Token")
            }
            
            return request
        }
    }
}