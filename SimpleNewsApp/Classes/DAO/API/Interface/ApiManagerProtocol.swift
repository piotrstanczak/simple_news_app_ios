//
//  ApiManagerProtocol.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 08/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

typealias Success = JsonObject -> ()
typealias Failure = NSError -> ()

protocol ApiManagerProtocol {    
    init(session: URLSessionProtocol)
    func perform(withProvider provider: Providable, success: Success, failure: Failure)
}