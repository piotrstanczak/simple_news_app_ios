//
//  ApiManager.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 24/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

// TODO: Change this as a failure result param
enum ApiError: ErrorType {
    case NetworkError
}

final class ApiManager: ApiManagerProtocol {
    
    private let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = NSURLSession.sharedSession()) {
        self.session = session
    }
    
    func perform(withProvider provider: Providable, success: Success, failure: Failure) {
        self.session.dataTaskWithRequest(provider.URLRequest) { (data, response, error) in
            
            log("data: \(data)")
            log("response: \(response)")
            log("error: \(error)")
            
            if let error = error {
                failure(error)
            } else if let response = response as? NSHTTPURLResponse where 200...299 ~= response.statusCode, let data = data {
                do {
                    let JSON = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? JsonObject
                    log("JSON: \(JSON)")
                    success(JSON!)
                } catch let error as NSError {
                    failure(error)
                }
            } else {
                // TODO: Replace with enum
                failure(NSError(domain: "error", code: 0, userInfo: nil))
            }

            
            /*if let data = data {
                do {
                    let JSON = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? JsonObject
                    // log("JSON: \(JSON)")
                    success(JSON!)
                } catch let error as NSError {
                    failure(error)
                }
            } else if let error = error {
                log("as I expected!!!")
                failure(error)
            }*/
        }.resume()
    }
}