//
//  Descriptable.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 08/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import CoreData

enum DescribtableFormat: Character {
    case Tab = "\t"
    case Space = " "
    case LineFeed = "\n"
}

protocol Describtable {
    var describePropertiesInline: String { get }
    var describePropertiesInMultiline: String { get }
    func describePropertiesWithFormat(format: DescribtableFormat) -> String
}

extension Describtable where Self: NSObject {
    
    var describePropertiesInline: String {
        let format: DescribtableFormat = .Tab
        return describePropertiesWithFormat(format)
    }
    
    var describePropertiesInMultiline: String {
        let format: DescribtableFormat = .LineFeed
        return describePropertiesWithFormat(format)
    }
    
    func describePropertiesWithFormat(format: DescribtableFormat = .Tab) -> String {
        var desc = "\(String(self.dynamicType)): \(format.rawValue)"
        let mirror = Mirror(reflecting: self)
        
        for child in mirror.children {
            desc += "\(child.label!) = \(child.value)\(format.rawValue)"
        }
        
        return desc
    }
}

extension Describtable where Self: NSManagedObject {
    
    var describePropertiesInline: String {
        let format: DescribtableFormat = .Tab
        return describePropertiesWithFormat(format)
    }
    
    var describePropertiesInMultiline: String {
        let format: DescribtableFormat = .LineFeed
        return describePropertiesWithFormat(format)
    }
    
    func describePropertiesWithFormat(format: DescribtableFormat = .Tab) -> String {
        var desc = "\(String(self.dynamicType)): \(format.rawValue)"
        
        for attributeKey in self.entity.attributesByName.keys {
            if let value = self.valueForKeyPath(attributeKey) {
                desc += "\(attributeKey) = \(value)\(format.rawValue)"
            }
        }
        
        return desc
    }
}