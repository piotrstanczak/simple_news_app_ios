//
//  KeychainManager.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 06/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit
import Security

// Identifiers
let serviceIdentifier = "MySerivice"
// let userAccount       = "authenticatedUser"
let accessGroup       = "MySerivice"

// Arguments for the keychain queries
let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

class KeychainService {
    
    private static let _instance = KeychainService()
    
    static func sharedService() -> KeychainService {
        return _instance
    }
    
    class func saveToken(text: String, forUser user: String) {
        log("")
        self.save(serviceIdentifier, data: text, forUser: user)
    }
    
    class func loadToken(forUser user: String) -> String? {
        log("")
        return self.load(serviceIdentifier, forUser: user)
    }
    
    class func deleteToken(text: String, forUser user: String) {
        log("")
        self.delete(serviceIdentifier, data: text, forUser: user)
    }
    
    private class func delete(service: NSString, data: NSString, forUser user: String) {
        if let dataFromString: NSData = data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            
            let query: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, user, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
            
            // Delete any existing items
            SecItemDelete(query as CFDictionaryRef)
        }
    }
    
    private class func save(service: NSString, data: NSString, forUser user: String) {
        if let dataFromString: NSData = data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            
            let query: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, user, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])
            
            // Delete any existing items
            SecItemDelete(query as CFDictionaryRef)
            
            // Add the new keychain item
            let status: OSStatus = SecItemAdd(query as CFDictionaryRef, nil)
            log("Save status: \(status)")
        }
    }
    
    private class func load(service: NSString, forUser user: String) -> String? {
        
        let query: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, user, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])
        
        var result: AnyObject?
        let status = withUnsafeMutablePointer(&result) { SecItemCopyMatching(query, UnsafeMutablePointer($0)) }
        
        switch status {
        case errSecSuccess:
            guard let data = result as? NSData, let string = NSString(data: data, encoding: NSUTF8StringEncoding) as? String else { return nil }
            return string
            
        case errSecItemNotFound:
            log("Item not found")
            return nil
        default:
            return nil
        }
    }
}