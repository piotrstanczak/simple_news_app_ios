//
//  ManagedObjectJsonParser.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 08/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import CoreData

public typealias JsonObject = [String: AnyObject]

public protocol Parsable {
    //MARK: properties
    static var parser: Parser { get }
    var entity: NSEntityDescription { get }
    
    //MARK: methods
    func refresh(json: JsonObject)
    func setValue(value: AnyObject?, forKey key: String)
}

public class Parser {
    private var _mappingDict: [String: String]
    
    
    //MARK: initializers
    public init() {
        _mappingDict = [String: String]()
    }
    
    public convenience init(mappingDict: [String: String]) {
        self.init()
        _mappingDict = mappingDict
    }
    
    
    //MARK: public methods
    public func addProperty(name name: String) {
        _mappingDict[name] = name
    }
    
    public func addProperty(name name: String, serializedPath: String) {
        _mappingDict[name] = serializedPath
    }
    
    public func parse(json json: JsonObject, toObject refObject: Parsable) {
        for attributeKey in refObject.entity.attributesByName.keys {
            let attributeDescription = refObject.entity.propertiesByName[attributeKey]! as! NSAttributeDescription

            let attributeClassName = attributeDescription.attributeValueClassName
            let attributeType = attributeDescription.attributeType
            
            if let serializedPath = _mappingDict[attributeKey] {
                let keyArray = serializedPath.characters.split { $0 == "/" }.map { String($0) }
                var jsonDict = json
                
                for key in keyArray {
                    if let value: AnyObject = jsonDict[key] {
                        if (key != keyArray.last) {
                            jsonDict = (value as? JsonObject) ?? JsonObject()
                        } else {
                            // print("propertyName: \(attributeKey)  serializedName: \(serializedPath)  propertyType: \(attributeClassName)  value: \(value) attributeType: \(attributeType)")
                            
                            switch (attributeType)
                            {
                            case .ObjectIDAttributeType:
                                
                                print("ObjectIDAttributeType")
                                
                            case .StringAttributeType:
                                
                                if let stringValue = value as? String
                                {
                                    refObject.setValue(stringValue, forKey: attributeKey)
                                }
                                else
                                {
                                    print("JSON PARSER ERROR: expected type '\(attributeClassName)', received type '\(attributeType.dynamicType)'")
                                }
                                
                            case .BooleanAttributeType:
                                
                                if let boolValue = value as? Bool
                                {
                                    refObject.setValue(boolValue, forKey: attributeKey)
                                }
                                else
                                {
                                    print("JSON PARSER ERROR: expected type '\(attributeClassName)', received type '\(attributeType.dynamicType)'")
                                }
                                
                            case .Integer16AttributeType,
                                    .Integer32AttributeType,
                                    .Integer64AttributeType,
                                    .DecimalAttributeType,
                                    .DoubleAttributeType,
                                    .FloatAttributeType:
                                
                                if let numberValue = value as? Double
                                {
                                    refObject.setValue(numberValue, forKey: attributeKey)
                                }
                                else
                                {
                                    print("JSON PARSER ERROR: expected type '\(attributeClassName)', received type '\(attributeType.dynamicType)'")
                                }
                                
                            default:
                                break
                            }
                        }
                    }
                }
            
            }
        }
    
    }
}
