//
//  Fetchable.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 08/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import CoreData

enum FetchableError: ErrorType {
    case Empty
}

public protocol Fetchable
{
    associatedtype FetchableType: NSManagedObject
    
    static func entityName() -> String
    static func context() -> NSManagedObjectContext
    static func identifier() -> String?
    
    static func create(withJSON JSON: JsonObject) -> FetchableType?
    static func singleObjectInContext(predicate: NSPredicate?, sortedBy: String?, ascending: Bool) throws -> FetchableType
    static func fetch(predicate: NSPredicate?, sortedBy: String?, ascending: Bool, withLimit limit: Int?, withOffset offset: Int?) throws -> [FetchableType]
    static func objectCountInContext(predicate: NSPredicate?) -> Int
    static func fetchRequest(predicate: NSPredicate?, sortedBy: String?, ascending: Bool, withLimit limit: Int?, withOffset offset: Int?) -> NSFetchRequest
    
    func delete() -> Bool
}

public extension Fetchable where Self : NSManagedObject, FetchableType == Self
{
    static func entityName() -> String {
        return NSStringFromClass(self).componentsSeparatedByString(".").last!
    }
    
    // MARK: default initializer
    
    public init(context: NSManagedObjectContext)
    {
        let entity =  NSEntityDescription.entityForName(FetchableType.entityName(), inManagedObjectContext: context)!
        self.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    
    // MARK: create
    
    public static func findOrCreateInContext(context: NSManagedObjectContext, withIdentifier identifier:String?) -> FetchableType
    {
        var entity: FetchableType?
        
        do {
            var predicate: NSPredicate? = nil
            
            if identifier != nil && identifier != "" && FetchableType.identifier() != nil && FetchableType.identifier() != ""
            {
                predicate = NSPredicate(format: "%K == %@", FetchableType.identifier()!, identifier!)
            }
            
            log("")
            
            entity = try FetchableType.singleObjectInContext(predicate)
            log("   1 :\(entity)")
            
        } catch FetchableError.Empty {
            
            entity = FetchableType(context: FetchableType.context())
            log("   2 :\(entity)")
            
        } catch {
            print("Something went wrong!")
        }
        
        return entity!
    }
    
    // MARK: Fetch
    
    public static func singleObjectInContext(predicate: NSPredicate? = nil, sortedBy: String? = nil, ascending: Bool = false) throws -> FetchableType
    {
        let managedObjects: [FetchableType] = try fetch(predicate, sortedBy: sortedBy, ascending: ascending)
        guard managedObjects.count > 0 else { throw FetchableError.Empty }
        
        return managedObjects.first!
    }
    
    static func fetch(predicate: NSPredicate? = nil, sortedBy: String? = nil, ascending: Bool = false, withLimit limit: Int? = nil, withOffset offset: Int? = nil) throws -> [FetchableType]
    {
        let request = fetchRequest(predicate, sortedBy: sortedBy, ascending: ascending, withLimit: limit, withOffset: offset)
        
        let fetchResults = try FetchableType.context().executeFetchRequest(request)
        
        return fetchResults as! [FetchableType]
    }
    
    static func fetchRequest(predicate: NSPredicate? = nil, sortedBy: String? = nil, ascending: Bool = false, withLimit limit: Int? = nil, withOffset offset: Int? = nil) -> NSFetchRequest
    {
        let context = FetchableType.context()
        let request = NSFetchRequest()
        // request.includesPendingChanges = false
        
        let entity = NSEntityDescription.entityForName(FetchableType.entityName(), inManagedObjectContext: context)
        request.entity = entity
        
        if predicate != nil {
            request.predicate = predicate
        }
        
        if (sortedBy != nil) {
            let sort = NSSortDescriptor(key: sortedBy, ascending: ascending)
            let sortDescriptors = [sort]
            request.sortDescriptors = sortDescriptors
        }
        
        if limit != nil
        {
            request.fetchLimit = limit!
        }
        
        if offset != nil
        {
            request.fetchOffset = offset!
        }
        
        return request
    }
    
    static func objectCountInContext(predicate: NSPredicate? = nil) -> Int
    {
        let request = fetchRequest(predicate)
        let error: NSErrorPointer = nil;
        let count = FetchableType.context().countForFetchRequest(request, error: error)
        guard error != nil else {
            NSLog("Error retrieving data %@, %@", error, error.debugDescription)
            return 0;
        }
        
        return count;
    }
    
    static func countInContext() -> Int
    {
        do {
            let items: [FetchableType] = try FetchableType.fetch(nil, sortedBy: nil, ascending: true)
            return items.count
        } catch let error as NSError {
            print("Could not count \(error), \(error.userInfo)")
            return 0
        }
    }
    
    func delete() -> Bool
    {
        let context = FetchableType.context()
        context.deleteObject(self)
        
        // FIXME: Dodać properties andSave = i w zależności od niego zapisywać
        // Przyda się jeśli będziemy usuwać wszystkie elementy i savewować dopiero po usunieciu listy
        do {
            try context.save()            
            return true
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
    }
    
    static func deleteAll(withPersistentStoreCoordinator persistentStoreCoordinator: NSPersistentStoreCoordinator, fromContext context: NSManagedObjectContext, withOffset offset: Int? = nil, sortedBy: String? = nil, predicate: NSPredicate? = nil) -> Bool
    {
        if #available(iOS 9.0, *)
        {
            let request = fetchRequest(predicate, sortedBy: sortedBy, ascending: true, withOffset: offset)
            
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
            let error: NSErrorPointer = nil;
            let count = FetchableType.context().countForFetchRequest(request, error: error)
            log("deleteAll count: \(count)")
            do {
                try persistentStoreCoordinator.executeRequest(deleteRequest, withContext: context)
                // try context.executeRequest(deleteRequest)
                
                return true
                
            } catch let error as NSError {
                print("Could not deleteAll \(error), \(error.userInfo)")
            }
            
        } else {
            do {
                let items: [FetchableType] = try FetchableType.fetch(predicate, sortedBy: sortedBy, ascending: true, withOffset: offset)
                
                log("will be deleted: \(items.count)")
                
                for item in items
                {
                    // log("\((item as! News).title)")
                    item.delete()
                }
                
                return true
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        
        return false
    }
}

extension Array where Element: Fetchable {
    
    public func delete() {
        for item in self {
            item.delete()
        }
    }
    
}

extension Array where Element: Fetchable {
    
    public static func create(elements: [JsonObject], ignoreInvalidObjects: Bool = true) throws -> [Element] {
        
        guard elements.count > 0 else {
            throw FetchableError.Empty
        }
        
        if ignoreInvalidObjects {
            // log("elements: \(elements)")
            // log("elements: \(elements.count)")
            let newElements = elements.flatMap{ Element.create(withJSON: $0) as? Element }
            // log("new elements: \(newElements.count)")
            return newElements
        } else {
            return elements.map{ Element.create(withJSON: $0) as! Element }
        }
    }
}