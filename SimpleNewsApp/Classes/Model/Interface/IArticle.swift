//
//  IArticle.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 27/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

enum ArticleType: Int {
    case Regular = 0
    case Search  = 1
}

protocol IArticle {
    var id: String  { set get }
    var title: String { set get }
    var lead: String { set get }
    var content: String { set get }
    var type: Int { set get }
}