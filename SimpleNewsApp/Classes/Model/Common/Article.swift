//
//  Article.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 24/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import CoreData

class Article: NSManagedObject, IArticle {
    @NSManaged var id: String
    @NSManaged var title: String
    @NSManaged var lead: String
    @NSManaged var content: String
    var type: Int = ArticleType.Regular.rawValue
}

// MARK: Fetchable protocol

extension Article: Fetchable {
    typealias FetchableType = Article
    
    class func identifier() -> String? {
        return "id"
    }
    
    class func context() -> NSManagedObjectContext {
        return CoreDataManager.sharedManager().managedObjectContext
    }
    
    class func create(withJSON JSON: JsonObject) -> FetchableType? {
        if let inforId = JSON["objectId"] as? String {
            let news = Article.findOrCreateInContext(FetchableType.context(), withIdentifier: inforId)
            news.type = ArticleType.Regular.rawValue
            news.refresh(JSON)
            return news
        }
        return nil
    }
}

//MARK: Parsable protocol

extension Article: Parsable {
    
    static var parser: Parser {
        let parser = Parser()
        parser.addProperty(name: "id",             serializedPath: "objectId")
        parser.addProperty(name: "title")
        parser.addProperty(name: "lead")
        parser.addProperty(name: "content")
        return parser
    }
    
    func refresh(json: JsonObject) {
        Article.parser.parse(json: json, toObject: self)
    }
}

//MARK: Descriptable protocol

extension Article: Describtable {
    override var description: String {
        return describePropertiesWithFormat()
    }
}