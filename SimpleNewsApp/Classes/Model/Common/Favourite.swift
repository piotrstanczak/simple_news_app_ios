//
//  Favourite.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 01/07/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import CoreData

class Favourite: NSManagedObject {
    @NSManaged var id: String
    @NSManaged var userId: String
    @NSManaged var articleId: String
}

// MARK: Fetchable protocol

extension Favourite: Fetchable {
    typealias FetchableType = Favourite
    
    static func identifier() -> String? {
        return "id"
    }
    
    static func context() -> NSManagedObjectContext {
        return CoreDataManager.sharedManager().managedObjectContext
    }
    
    static func create(withJSON JSON: JsonObject) -> FetchableType? {
        if let _ = JSON["objectId"] as? String
        {
            let favourite = Favourite(context: Favourite.context())
            favourite.refresh(JSON)
            return favourite
        }
        return nil
    }
}

// MARK: Parsable protocol

extension Favourite: Parsable {
    
    static var parser: Parser {
        let parser = Parser()
        parser.addProperty(name: "id", serializedPath: "objectId")
        parser.addProperty(name: "userId")
        parser.addProperty(name: "articleId")
        return parser
    }
    
    func refresh(json: JsonObject) {
        Favourite.parser.parse(json: json, toObject: self)
    }
}

// MARK: Descriptable protocol

extension Favourite: Describtable {
    override var description: String {
        return describePropertiesWithFormat()
    }
}