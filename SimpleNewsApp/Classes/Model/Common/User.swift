//
//  User.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 28/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject {
    @NSManaged var id: String
    @NSManaged var username: String
    
    private var _sessionToken: String?
    var sessionToken: String? {
        get {
            return KeychainService.loadToken(forUser: username)
        }
        set {
            if _sessionToken != nil && newValue == nil {
                KeychainService.deleteToken(_sessionToken!, forUser: username)
            } else if newValue != nil {
                _sessionToken = newValue
                KeychainService.saveToken(newValue!, forUser: username)
            }
        }
    }
    
    func deleteUser() -> Bool {
        self.sessionToken = nil
        return self.delete()
    }
}

// MARK: Fetchable protocol

extension User: Fetchable {
    typealias FetchableType = User
    
    static func identifier() -> String? {
        return "id"
    }
    
    static func context() -> NSManagedObjectContext {
        return CoreDataManager.sharedManager().managedObjectContext
    }
    
    static func create(withJSON JSON: JsonObject) -> FetchableType? {
        if let inforId = JSON["objectId"] as? String {
            let user = User.findOrCreateInContext(FetchableType.context(), withIdentifier: inforId)
            user.refresh(JSON)
            
            if let sessionToken = JSON["sessionToken"] as? String {
                user.sessionToken = sessionToken
            }
            
            return user
        }
        return nil
    }    
}

// MARK: Parsable protocol

extension User: Parsable {
    
    static var parser: Parser {
        let parser = Parser()
        parser.addProperty(name: "id", serializedPath: "objectId")
        parser.addProperty(name: "username")
        return parser
    }
    
    func refresh(json: JsonObject) {
        User.parser.parse(json: json, toObject: self)
    }
}

// MARK: Descriptable protocol

extension User: Describtable {
    override var description: String {
        return describePropertiesWithFormat()
    }
}