//
//  Search.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 30/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import CoreData

class Search: Article {
    
    typealias FetchableType = Search
    
    override class func context() -> NSManagedObjectContext {
        return CoreDataManager.sharedManager().privateObjectContext
    }
    
    override class func create(withJSON JSON: JsonObject) -> Search? {
        if let _ = JSON["objectId"] as? String
        {
            let news = Search(context: Search.context())
            news.type = ArticleType.Search.rawValue
            news.refresh(JSON)
            return news
        }
        return nil
    }
}

