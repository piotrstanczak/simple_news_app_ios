//
//  ArticleView.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 27/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit

class ArticleView: UIView {
    @IBOutlet weak var webView: UIWebView!
    
    var favouriteIcon = UIImage(named: "FavouriteIcon")
    var favouriteMarkedIcon = UIImage(named: "FavouriteMarkedIcon")
}