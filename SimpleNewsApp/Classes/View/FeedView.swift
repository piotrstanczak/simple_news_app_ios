//
//  FeedView.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 27/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation
import UIKit

class FeedView: UIView {
    @IBOutlet weak var tableView: UITableView!
    var searchBar = UISearchBar()
    var logoImageView = UIImageView(image: UIImage(named: "Icon"))
    var userLoggedIcon = UIImage(named: "UserLoggedIcon")
    var userNotLoggedIcon = UIImage(named: "UserNotLoggedIcon")
    
    
    func setupSearchBar() {
        self.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        self.searchBar.showsCancelButton = true
        self.searchBar.placeholder = "search..."
    }
}