//
//  define.swift
//  SimpleNewsApp
//
//  Created by Piotr Stańczak on 24/06/16.
//  Copyright © 2016 Piotr Stańczak. All rights reserved.
//

import Foundation

func log(message: String, filename: String = #file, line: Int = #line, funct: String = #function) {
    print("\((filename as NSString).lastPathComponent):\(line) \(funct): \(message)")
}
